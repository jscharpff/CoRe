# CoRe

The stand-alone Conditional Return Policy Search, or CoRe, solver as used in the article "J. Scharpff, D. M. Roijers, F. A. Oliehoek, M. T. J. Spaan, and M. M. de Weerdt. Solving transition-independent multi-agent MDPs with sparse interactions."

This project has moved to the [TU Delft Algorithmics group repository](https://github.com/AlgTUDelft/core-solver) on GitHub.